#  PV251 Project: Smoking, obesity and alcohol consumption
## Info
- Author: Terézia Slanináková (445526)
- Date: 24.12.2019

## How to run
- Prerequisite: python3 installed
- `pip install -r requirements.txt` will install the dependencies
- `python index.py` will launch the server on http://127.0.0.1:8050/

## Description
The data shows oecd healthrisk data of smoking, alcohol consumption and overweight/obese population throughout the world (and years). The data was published for [Health at a Glance](http://dx.doi.org/10.1787/19991312), original data may be found [here](https://data.oecd.org/healthrisk/daily-smokers.htm#indicator-chart).

1. Percentage of daily smokers (15+ yrs) | men-women-total | 1960-2018
2. Liters of alcohol / capita / year (15+ yrs) | total | 1960-2018
3. Percentage of population (15+ yrs) | measured-selfreported | 1978-2018

The main programming part is in `assets/main.py`.

## Demo
![demo](demo.gif)